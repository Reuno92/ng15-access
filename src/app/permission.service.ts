import { Injectable } from '@angular/core';
import {PermissionType} from "./model/type/permission.type";

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  private static readonly admin: boolean = false;
  constructor() {}

  getPermission(): PermissionType {
    return {
      admin: PermissionService.admin,
    };
  }
}
