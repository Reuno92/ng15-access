import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProtectedComponent } from './protected/protected.component';
import { NotAccesibleComponent } from './not-accessible/not-accesible.component';
import {NgOptimizedImage} from "@angular/common";
import { HomeComponent } from './home/home.component';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    AppComponent,
    ProtectedComponent,
    NotAccesibleComponent,
    HomeComponent
  ],
    imports: [
        BrowserModule,
        RouterModule,
        AppRoutingModule,
        NgOptimizedImage
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
