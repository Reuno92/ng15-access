import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {PermissionService} from "./permission.service";
import {PermissionType} from "./model/type/permission.type";

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

  constructor(
      private router: Router,
      private permissionService: PermissionService,
  ) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const PERMIT: keyof PermissionType = route.data['permit'];

    if (!this.permissionService.getPermission()[PERMIT]) {
      this.router.navigate(["NotAccessible"], { state: {
        message: 'You have not permission…',
      } });
      return false;
    }

    return true;
  }

}
