import { Component } from '@angular/core';
import {PermissionService} from "../permission.service";

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.scss'],
  providers: [PermissionService]
})
export class ProtectedComponent {

    constructor(
    ) {}
}
