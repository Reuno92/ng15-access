import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProtectedComponent} from "./protected/protected.component";
import {NotAccesibleComponent} from "./not-accessible/not-accesible.component";
import {PermissionGuard} from "./permission.guard";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [
        PermissionGuard
    ],
    data: {
      permit: 'admin',
    }
  },
  {
    path: 'NotAccessible',
    component: NotAccesibleComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
