import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {state} from "@angular/animations";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

    constructor(private router: Router) {
    }
    unProtectedRoute() {
        this.router.navigate(['/protected'], { state: { permit: { user: true }}})
    }
}
