import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-not-accessible',
  templateUrl: './not-accesible.component.html',
  styleUrls: ['./not-accesible.component.scss']
})
export class NotAccesibleComponent {
  public errorMessage: string
  constructor(
      private router: Router,
  ) {
    const extra = this.router.getCurrentNavigation()?.extras?.state;

    if (extra && extra?.['message']) {
      this.errorMessage = extra['message'];
    } else {
      this.errorMessage = 'There is an unknown error';
    }
  }

}
