# Activated Route

![the good, the bad and the ugly](https://media.tenor.com/AZh0pzx9MssAAAAM/the-good-the-bad-and-the-ugly-clint-eastwood.gif)

In this repo, I explain a little how to activated route via a service and GuardService

## Explanation
In your source app, you have:

`Permission.Guard` take data pass in route into `permit` property.

```typescript
const PERMIT: keyof PermissionType = route.data['permit'];
```

which have a specific type:

```typescript
export type PermissionType = {
  admin: boolean
}
```

and it seeks this `keyof` PermissionType into to PermissionService

> Of course, you must search all permission in your data and store user permissions with a login page,
> but it's not the goal of our this repository sorry young Padawan. 
> ![](https://www.tomsguide.fr/content/uploads/sites/2/2021/05/20210506-star-wars-the-bad-batch-qui-est-le-padawan-qui-apparat-dans-lpisode-1-docx.jpg.webp)

```typescript
if (!this.permissionService.getPermission()[PERMIT]) {
  this.router.navigate(["NotAccessible"], { state: {
    message: 'You have not permission…',
  } });
  return false;
}
```

## Demonstration

```bash
  ng serve
```

In the permission.service.ts, change the value of `PermissionService.admin`, if you granted a rule.
```typescript
export class PermissionService {
    private static readonly admin: boolean = true;
}
```

If you have an error, you have replaced by mistake all code by above… Call the command `git reset --hard`.  
![Joey winks](https://media.tenor.com/gaiKIUG-zNoAAAAC/friends-joey-tribbiani.gif)

```bash
open -a "Google chrome" "http://localhost:4200/"
```

And click on the button. if your `PermissionService.Admin` is true, Colin Jost congratule you. Else he's mocking you. 

![](https://media.tenor.com/xvo8-YQ78P0AAAAC/porky-pig.gif)
